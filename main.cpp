//
//  main.cpp
//  Prettree
//
//  Created by Xu Xu on 11/9/13.
//  Copyright (c) 2013 XUXU.name. All rights reserved.
//

#define ENABLE_PRINT

#include "Prettree.cpp"

int randomInt(int min, int max)
{
    if (min > max) return 0;
    if (min == max) return min;
    return min + rand() % (max - min + 1);
}

int main(int argc, const char * argv[])
{
    // initial random
    srand((unsigned)time(0));
    
    Prettree<int> *tree = new Prettree<int>;
    
    int count = randomInt(10, 20);
    for (int i = 0; i < count; ++i)
    {
        tree->insert(randomInt(0, 100));
    }
    
    tree->printInOrder();
    tree->printInOrder2();
    
    tree->printPreOrder();
    tree->printPreOrder2();
    
    tree->printPostOrder();
    tree->printPostOrder2();
    
    delete tree;
    
    
//    
//    Prettree<int> *special = new Prettree<int>;
//    special->insert(10);
//    special->insert(5);
//    special->insert(3);
//    special->insert(6);
//    special->insert(4);
//    special->insert(8);
//    special->insert(9);
//    special->insert(15);
//    special->insert(13);
//    special->insert(20);
//    special->insert(18);
//    special->insert(22);
//    special->insert(21);
//    
//    tree->printPreOrder();
//    tree->printPreOrder2();
    
    return 0;
}

