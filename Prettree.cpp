//
//  Prettree.cpp
//  Prettree
//
//  Created by Xu Xu on 11/9/13.
//  Copyright (c) 2013 XUXU.name. All rights reserved.
//

#ifdef ENABLE_PRINT
#include <iostream>
using namespace std;
#endif


#include <stack>
#include <unordered_map>
using namespace std;

// ValueType should have =, <, >, == <<
template <class ValueType>
class Prettree
{
public:
    // Node of Tree
    struct Node
    {
        Node():left(nullptr),right(nullptr), count(1) {}
        ValueType value;
        Node * left;
        Node * right;
        unsigned count;
    };
    
    // Construction
    Prettree()
    {
        root = nullptr;
    }
    
    // Deconstruction
    ~Prettree()
    {
        deleteNode(root);
        root = nullptr;
    }
    
    // Insert value
    void insert(ValueType value)
    {
        Node * node = new Node;
        node->value = value;
        
        if (!root) root = node;
        else insertNode(root, node);
    }
    
#ifdef ENABLE_PRINT
    // print out my pretty tree
    void printInOrder()
    {
        if (!root) return;
        printNodeInOrder(root);
        cout << endl;
    }
    
    // print out the tree without recurision
    void printInOrder2()
    {
        stack<Node*> needPushLeft;
        stack<Node*> needPushRight;
        
        if (!root) return;
        needPushLeft.push(root);
        
        while (true)
        {
            Node * node;
            
            if (!needPushLeft.empty())
            {
                node = needPushLeft.top();
                needPushLeft.pop();
                
                if (node->left)
                {
                    needPushLeft.push(node->left);
                    needPushRight.push(node);
                    continue;
                }
            }
            else if (!needPushRight.empty())
            {
                node = needPushRight.top();
                needPushRight.pop();
            }
            else break;
            
            printNodeValue(node);
            
            if (node->right)
            {
                needPushLeft.push(node->right);
            }
        }
        
        cout << endl;
    }
    
    void printPreOrder()
    {
        if (!root) return;
        printNodePreOrder(root);
        cout << endl;
    }
    
    // without recursion
    void printPreOrder2()
    {
        stack<Node*> s;
        
        if (!root) return;
        s.push(root);
        
        while (!s.empty())
        {
            Node * node = s.top();
            s.pop();
            
            printNodeValue(node);
            
            if (node->right) s.push(node->right);
            if (node->left) s.push(node->left);
        }
        
        cout<<endl;
    }
    
    void printPostOrder()
    {
        if (!root) return;
        printNodePostOrder(root);
        cout << endl;
    }
    
    void printPostOrder2()
    {
        stack<Node*> s;
        unordered_map<Node*, int> flags;
        if (!root) return;
        s.push(root);
        flags[root] = 0;
        
        while(!s.empty())
        {
            Node* node = s.top();
            if (flags[node] == 0)
            {
                // need push left
                if (node->left)
                {
                    s.push(node->left);
                    flags[node->left] = 0;
                }
                flags[node] = 1;
            }
            else if (flags[node] == 1)
            {
                // need push right
                if (node->right)
                {
                    s.push(node->right);
                    flags[node->right] = 0;
                }
                flags[node] = 2;
            }
            else
            {
                printNodeValue(node);
                s.pop();
            }
        }
        
        cout<<endl;
    }
    
#endif
    
private:
    
#pragma mark - Operations
    
    void insertNode(Node * current, Node *& node)
    {
        if (node->value == current->value)
        {
            ++current->count;
        }
        else if (node->value < current->value)
        {
            if (current->left) insertNode(current->left, node);
            else current->left = node;
        }
        else // >
        {
            if (current->right) insertNode(current->right, node);
            else current->right = node;
        }
    }
    
    void deleteNode(Node *& node)
    {
        if (!node) return;
        deleteNode(node->left);
        deleteNode(node->right);
        delete node;
    }
    
#pragma mark - Traversals
    
#ifdef ENABLE_PRINT
    
    void printNodeInOrder(Node *& node)
    {
        if (node->left) printNodeInOrder(node->left);
        printNodeValue(node);
        if (node->right) printNodeInOrder(node->right);
    }
    
    void printNodePreOrder(Node *& node)
    {
        printNodeValue(node);
        if (node->left) printNodePreOrder(node->left);
        if (node->right) printNodePreOrder(node->right);
    }
    
    void printNodePostOrder(Node *& node)
    {
        if (node->left) printNodePostOrder(node->left);
        if (node->right) printNodePostOrder(node->right);
        printNodeValue(node);
    }
    
    void printNodeValue(Node *& node)
    {
        cout << node->value;
        if (node->count > 1) cout << "(" << node->count << ")";
        if (node == root) cout << "(R)";
        cout << " ";
    }
    
#endif
    
private:
    Node * root;
};